#!/bin/bash

_unwind-ows() {

  local output;
  local path;

  output=$((git add .) 2>&1);
  if [ $? -eq 0 ]; then
    echo "Successfully converted CRLF to LF in all files.";
    echo "Successfully ran `git add .`.";
    echo "Done.";
    return 0;
  fi;

  path="$(echo "${output}" | sed -e 's/^fatal: CRLF would be replaced by LF in (.*)\.$/\1/g')";
  if [ "${path}" = "${output}" ]; then
    echo "Regex failed. Could not auto-generate filename from stderr.";
    return 1;
  fi;

  if [ ! -e "${path}" ]; then
    echo "Regex failed. '${path}' does not exist.";
    return 1;
  fi;

  dos2unix "${path}";
  if [ $? -ne 0 ]; then
    echo "Failed to run \`dos2unix '${path}'\`.";
    return 1;
  fi;

  _unwind-ows;
  return $?;
};

_unwind-ows;