function getWager() {
  // return wager as an Int
  return parseInt(document.getElementById("starting_wager").value, 10);
}

function rollDice() {
  // Sum of two dice rolls
  return Math.ceil(Math.random() * 6) + Math.ceil(Math.random() * 6);
}

function updateResults(gameStats) {
  // Update the table data
  document.getElementById("total_rolls").innerHTML = "&nbsp" + gameStats.totalRolls;
  document.getElementById("max_won").innerHTML = "&nbsp" + gameStats.maxWon;
  document.getElementById("best_roll").innerHTML = "&nbsp" + gameStats.shouldHaveQuitRoll;
}

function showResultsTable(){
  // Make the results table visible
  document.getElementById("results_table").className = "show";
}

function playAgain() {
  // Set play button text to "Play Again"
  document.getElementById("play_button").setAttribute("value", "Play Again");
}

function initializeGameStats(wager) {
  // Set up gameStats object
  return {
    maxWon: wager,
    shouldHaveQuitRoll: 0,
    totalRolls: 0
  };
}

function runLuckySevensLoop(gameStats, wager) {

  while(wager > 0) {
    gameStats.totalRolls += 1;

    if(rollDice() === 7) {
      wager += 4;

      // Update game stats if new max is achieved
      if (wager > gameStats.maxWon) {
        gameStats.maxWon = wager;
        gameStats.shouldHaveQuitRoll = gameStats.totalRolls;
      }
    } else {
      wager -= 1;
    }
  }
}

function playLuckySevens() {

  var wager = getWager();
  var gameStats = initializeGameStats(wager);

  // Check if a wager was given
  if(wager) {
    runLuckySevensLoop(gameStats, wager);
  } else {
    // Alert player if no wager was entered
    alert("Please enter your wager.");
    return 0;
  }

  updateResults(gameStats);
  showResultsTable();
  playAgain();
}

