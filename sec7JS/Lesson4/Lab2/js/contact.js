// Requires jQuery, loaded in contact.html
function setError(error, msg) {

  if(error === "") {
    error = msg;
  } else {
    error = error + "\n" + msg;
  }
  return error;
}

function addErrorClass(elem) {

  elem.addClass("has-error")
}

function removeErrorClass(elem) {

  elem.removeClass("has-error");
}

function checkName(err) {

  var element = $("#name_form_group");
  removeErrorClass(element);

  var name = $("input:text[name=name]").val();

  if(name == "") {
    err = setError(err, "Please enter your name.")
    addErrorClass(element);
  }
  return err;
}

function checkContactInfo(err) {

  var emailElement = $("#email_form_group");
  var phoneElement = $("#phone_form_group");
  removeErrorClass(emailElement);
  removeErrorClass(phoneElement);

  var email = $("input:text[name=email]").val();
  var phone = $("input:text[name=phone]").val();

  if(email == "" && phone == "") {
    err = setError(err, "Please enter email or phone number.");
    addErrorClass(emailElement);
    addErrorClass(phoneElement);
  }
  return err
}

function checkInquiryDropdown(err) {

  var addlInfoGroup = $("#addl_info_form_group");
  removeErrorClass(addlInfoGroup);

  var inquiry = $("#inquiry").val();

  if(inquiry === "other") {
    err = checkAdditionalInfo(err);
  }
  return err;
}

function checkAdditionalInfo(err) {

  var addInfo = $("#addl_info").val();

  if(addInfo === "") {
    err = setError(err, "Add additional info about your inquiry.");
    var element = $("#addl_info_form_group");
    addErrorClass(element);
  }
  return err;
}

function checkContactDays(err) {
  var element = $("#best_contact_form_group");
  removeErrorClass(element);

  var numDaysSelected = $("input:checkbox[name=contact-day]:checked").length;

  if(numDaysSelected === 0) {
    err = setError(err, "Select at least one day to be contacted.");
    addErrorClass(element);
  }
  return err;
}

function checkContactForm(event) {
  var errorMsg = "";

  errorMsg = checkName(errorMsg);
  errorMsg = checkContactInfo(errorMsg);
  errorMsg = checkInquiryDropdown(errorMsg);
  errorMsg = checkContactDays(errorMsg);

  if(errorMsg != "") {
    alert(errorMsg);
    event.preventDefault();
    $("#contact_form_footnote").text("* Red fields are required");
  }
}

$("#contact_form").submit(checkContactForm);

