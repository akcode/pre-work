function playLS(betArray) {
  var currentBet = betArray[(betArray.length -1)];
  if(currentBet <= 0) {
    return betArray;
  } else {
    ((Math.ceil(Math.random() * 6) + Math.ceil(Math.random() * 6)) === 7) ?
      betArray.push(currentBet + 4) : betArray.push(currentBet - 1);
  }
  return playLS(betArray);
}

function playLuckySevens() {
  let betArray = playLS([parseInt(document.getElementById("starting_wager").value, 10)])
  document.getElementById("total_rolls").innerHTML = betArray.length - 1;
  document.getElementById("max_won").innerHTML = Math.max(...betArray);
  document.getElementById("best_roll").innerHTML = betArray.indexOf(Math.max(...betArray));
  document.getElementById("results_table").className = "show";
  document.getElementById("play_button").setAttribute("value", "Play Again");
}